#! /usr/bin/env python3

import rospy
from odas_ros.lib_odas_server_node import OdasServerNode

class OdasServerWithoutSSSNode(OdasServerNode):
    # WARN: if super.__init__ is called explicitly the overwrite does not work anymore
    def _verify_sss_configuration(self):
        return False

def main():
    rospy.init_node('odas_server_node')
    odas_server_node = OdasServerWithoutSSSNode()
    odas_server_node.run()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass

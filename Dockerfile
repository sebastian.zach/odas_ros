FROM condaforge/mambaforge

SHELL ["/bin/bash", "-c"]
RUN mamba init bash

RUN mamba create -y -n ros_env python=3.9

RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh &&\
    mamba activate ros_env && conda config --env --add channels conda-forge && \
    conda config --env --add channels robostack-staging && \
    conda config --env --remove channels defaults;  \
    mamba install -y ros-noetic-desktop compilers cmake pkg-config make ninja colcon-common-extensions catkin_tools

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC
RUN apt-get update --allow-releaseinfo-change && apt-get install -y --no-install-recommends libgl1 ssh pulseaudio

RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    rosdep init && \
    rosdep update

RUN apt-get update --allow-releaseinfo-change

RUN apt-get install -y --no-install-recommends mumble-server golang libalut-dev libopus-dev cmake gcc build-essential alsa-utils pulseaudio ffmpeg libsm6 libxext6
RUN GO111MODULE=on go get 'github.com/farmergreg/barnard'

RUN apt-get install -y --no-install-recommends libfftw3-dev libconfig-dev libasound2-dev libpulse-dev libgfortran-*-dev perl gfortran texinfo libconfig-dev
RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    mamba install -y -c conda-forge libconfig && pip install libconf && \
    ln -s /usr/lib/x86_64-linux-gnu/alsa-lib/ /opt/conda/envs/ros_env/lib/

RUN mkdir -p /catkin_ws/src/odas_ros

RUN --mount=type=ssh cd /catkin_ws/src && mkdir ~/.ssh && ssh-keyscan git.rwth-aachen.de >> ~/.ssh/known_hosts && \
    git clone --depth=1 --branch main 'git@git.rwth-aachen.de:sebastian.zach/audio_utils.git' && \
    cd /catkin_ws/src/audio_utils && git submodule update --init --recursive

RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    cd /catkin_ws/src/audio_utils && pip install -r requirements.txt

COPY ./package.xml /catkin_ws/src/odas_ros/package.xml
COPY ./CMakeLists.txt /catkin_ws/src/odas_ros/CMakeLists.txt
COPY ./setup.py /catkin_ws/src/odas_ros/setup.py
COPY ./src /catkin_ws/src/odas_ros/src
COPY ./scripts /catkin_ws/src/odas_ros/scripts
COPY ./msg /catkin_ws/src/odas_ros/msg
COPY ./launch /catkin_ws/src/odas_ros/launch
COPY ./config /catkin_ws/src/odas_ros/config
COPY ./beam.cfg /catkin_ws/src/odas_ros/beam.cfg
COPY ./alsoftrc /catkin_ws/src/odas_ros/alsoftrc
COPY ./pulseaudio.client.conf /etc/pulse/client.conf
RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    cd /catkin_ws && rosdep update && rosdep install --from-paths src --ignore-src -r -y


RUN . /opt/conda/etc/profile.d/conda.sh && . /opt/conda/etc/profile.d/mamba.sh && mamba activate ros_env && \
    cd /catkin_ws && catkin_make

RUN echo "source /catkin_ws/devel/setup.bash" >> /root/.bashrc
RUN echo "mamba activate ros_env" >> /root/.bashrc
WORKDIR /catkin_ws
